<?php
/**
 * @file
 * my_feature_my_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function my_feature_my_page_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function my_feature_my_page_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function my_feature_my_page_node_info() {
  $items = array(
    'post' => array(
      'name' => t('Post'),
      'base' => 'node_content',
      'description' => t('Node for district post.'),
      'has_title' => '1',
      'title_label' => t('header'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
