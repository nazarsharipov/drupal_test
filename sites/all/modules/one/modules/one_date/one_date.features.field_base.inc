<?php
/**
 * @file
 * one_date.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function one_date_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_one_site_date_partial'
  $field_bases['field_one_site_date_partial'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_one_site_date_partial',
    'field_permissions' => array(
      'type' => 0,
    ),
    'foreign keys' => array(),
    'indexes' => array(
      'timestamp' => array(
        0 => 'timestamp',
      ),
      'timestamp_to' => array(
        0 => 'timestamp_to',
      ),
    ),
    'locked' => 0,
    'module' => 'partial_date',
    'settings' => array(
      'estimates' => array(
        'day' => array(
          '0|12' => 'The start of the month',
          '10|20' => 'The middle of the month',
          '18|31' => 'The end of the month',
        ),
        'hour' => array(
          '0|1' => 'Midnight',
          '6|12' => 'Morning',
          '6|18' => 'Day time',
          '12|13' => 'Noon',
          '12|18' => 'Afternoon',
          '18|6' => 'Night',
          '18|22' => 'Evening',
        ),
        'minute' => array(),
        'month' => array(
          '2|4' => 'Spring',
          '5|7' => 'Summer',
          '8|10' => 'Autumn',
          '11|1' => 'Winter',
        ),
        'second' => array(),
        'year' => array(
          '-60000|1600' => 'Pre-colonial',
          '1500|1599' => '16th century',
          '1600|1699' => '17th century',
          '1700|1799' => '18th century',
          '1800|1899' => '19th century',
          '1900|1999' => '20th century',
          '2000|2099' => '21st century',
        ),
      ),
      'minimum_components' => array(
        'from_estimates_day' => 0,
        'from_estimates_hour' => 0,
        'from_estimates_minute' => 0,
        'from_estimates_month' => 0,
        'from_estimates_second' => 0,
        'from_estimates_year' => 0,
        'from_granularity_day' => 0,
        'from_granularity_hour' => 0,
        'from_granularity_minute' => 0,
        'from_granularity_month' => 0,
        'from_granularity_second' => 0,
        'from_granularity_timezone' => 0,
        'from_granularity_year' => 0,
        'to_estimates_day' => 0,
        'to_estimates_hour' => 0,
        'to_estimates_minute' => 0,
        'to_estimates_month' => 0,
        'to_estimates_second' => 0,
        'to_estimates_year' => 0,
        'to_granularity_day' => 0,
        'to_granularity_hour' => 0,
        'to_granularity_minute' => 0,
        'to_granularity_month' => 0,
        'to_granularity_second' => 0,
        'to_granularity_timezone' => 0,
        'to_granularity_year' => 0,
        'txt_long' => 0,
        'txt_short' => 0,
      ),
    ),
    'translatable' => 0,
    'type' => 'partial_date_range',
  );

  return $field_bases;
}
