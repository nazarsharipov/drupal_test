<?php
function take_test($gr_id,$sub_id){
	 
	 
	 $query=db_select('test_questions','q');
	 $query->join('test_settings', 'ts', 'q.gr_id = ts.gr_id AND q.sub_id = ts.sub_id AND ts.sub_id = :sub_id', array(':sub_id' => $sub_id));
	  $query->condition('q.gr_id',$gr_id,'=')
			->condition('q.sub_id',$sub_id,'=')
 			->fields('q')
			->fields('ts');
			
			
	$count = $query->countQuery()->execute()->fetchField();
	
	
	if($count>0){
	$result=$query->execute();
	
	 
	
	
	$questions_id=array();
	
	$full_marks=0;
	$pass_percentage=0;
    $total_questions_no=1;
	
	$sn=1;
	foreach($result as $question){
	     //dpm($question);
		 
		 $full_marks=$question->full_marks;
		 $pass_percentage=$question->pass_percentage;
		 $header=array('S.No | Question');
		 $rows[]=array($sn .' | '.'<b>'.$question->title.'</b>');
		 
		 
		 /**options*////////////////
		 $options=array(); 
		 $rows2=array();
		  
		 $header2=array('','');
		 
		 $questions_id[]=$question->qid;
		 
		 
		 foreach(unserialize($question->ans_options) as $key=>$answer){
		 	 
			
		 	$rows2[]=array($answer,'<label style="width:100px;"><input type="radio" name="options['.$question->qid.']" value="'.$key.'" />
			<input type="hidden" name="total_questions_no" value="'.$total_questions_no.'"/>
			<input type="hidden" name="gr_id" value="'.$gr_id.'"/>
			<input type="hidden" name="sub_id" value="'.$sub_id.'"/>
			<input type="hidden" name="full_marks" value="'.$full_marks.'"/>
			<input type="hidden" name="pass_percentage" value="'.$pass_percentage.'"/>
			
			
			
			</label>');
		 	
		 
		 }
		 $options=theme('table',array('header'=>$header2,'rows'=>$rows2));
		 /** end options*///////////
		 
		 
	 	 $rows[]=array($options);
		 $rows[]=array('');
		 
		 $total_questions_no++;
  		$sn++;
	}
	
	 
	$data= '<div class="countdown"> </div>';
	 
  jquery_countdown_add(".countdown", array("until" => $question->duration*60, "onExpiry" => "finished"));
  drupal_add_js("function finished() { 
  
	 jQuery('#submit_take_test').trigger('click');  
  	//alert('You are done!'); 
  
  }", 'inline');
	 
	 $data.="<input type='hidden' name='questions' value='".serialize($questions_id)."'/>";
	 $data.=theme('table',array(
	 	'header'=>$header,
		'rows'=>$rows
	 ));
	 
	 function take_test_button($form,&$form_state,$data){ 
 	 
	  $form['data']=array(
	  	'#markup'=>$data
	  );
	   
	  
	  $form['submit']=array(
	  	'#type'=>'submit',
		'#value'=>'Submit',
		'#attributes'=>array('id'=>array('submit_take_test')),
	  );
	  
	  
	  return $form;
	 }
	
	  function take_test_button_submit($form,&$form_state){
	   
	  	global $user;
		
 	 	$selected=$form_state['input']['options'];
		
		$total_questions_no=$form_state['input']['total_questions_no'];
		
		$questions_id=$form_state['input']['questions'];
		
		$gr_id=$form_state['input']['gr_id'];
		$sub_id=$form_state['input']['sub_id'];
		$full_marks=$form_state['input']['full_marks'];
		$pass_percentage=$form_state['input']['pass_percentage'];
		
		  // dpm($form_state);
		 
		  
		  $right_ans_question_id=array();
		  $wrong_ans_question_id=array();
		  $right_ans='';
	 	  $wrong_ans='';
		
		 if(count($selected) >0){
		 
		  
		
 		  foreach($selected as $key=>$value){
		  	
			$query=db_select('test_questions','q');
			$query->fields('q')
			 	->condition('q.qid',$key,'=')
				->condition('q.right_ans',$value,'=');
				
			$result=$query->execute();
			 $questions=$result->fetchAssoc();

			
			  $count = $query->countQuery()->execute()->fetchField();

			 
			 
				if($count>0){
					//right answer
					
					$right_ans_question_id[]=$questions['qid'];
					$right_ans=$right_ans+1;
				}else{
					//wrong answer
					
					$wrong_ans_question_id[]=$questions['qid'];
					$wrong_ans=$wrong_ans+1;
				}
			
			 
			
 		  }
		  
		  
		  //selected
		  }else{
		  //no any option selected
		  
		  
		  }
		  
		//now calculate pass failed from test_settings table and write record to test_user_record
		
		$marks=($right_ans/$total_questions_no)*$full_marks;
		
		$percentage=($marks/$full_marks)*100;
		 
		
		$fields=array(
			'uid'=>$user->uid,
			'sub_id'=>$sub_id,
			'gr_id'=>$gr_id,
			'full_marks'=>$full_marks,
			'total_question_no'=>$total_questions_no,
			'total_ques_id_arr'=>$questions_id,
			'total_right_ans_no'=>$right_ans,
			'total_right_ans_id_arr'=>serialize($right_ans_question_id),
			'total_wrong_ans_no'=>$wrong_ans,
			'total_wrong_ans_id_arr'=>serialize($wrong_ans_question_id),
			'pass_percentage'=>$pass_percentage,
			'status'=>'completed'
		);
		
		if($percentage<$pass_percentage){
			$fields['remarks']=serialize(array('remarks'=>'failed','percentage'=>$percentage));
		}else{
			$fields['remarks']=serialize(array('remarks'=>'passed','percentage'=>$percentage));
		}
		
		
		 // dpm($fields);
		
		  drupal_write_record('test_user_record',$fields);
		
		//last inserted id
		$tur_id=$fields['tur_id'];
		 
		
		 
		 $form_state['redirect']=array(
	  	'user_dashboard/result',
	  	array(
		'query'=>array('tur_id'=>$tur_id)
		),
	  );
	  
	  	  
		  
	 }
	 
	 return drupal_get_form('take_test_button',$data);
	 
	 
	
	 
	 
	 
	 }else{
	 	return 'Sorry ! No Question found for this grade and subject';
	 }
	 
}