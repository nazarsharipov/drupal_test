<?php
function test_setting_add(){
	return drupal_get_form('test_setting_add_form');
}

	function test_setting_add_form($form, &$form_state){
			//list all subject terms////////////
	  $v2 = taxonomy_vocabulary_machine_name_load('test_subject');
  $terms2 = taxonomy_get_tree($v2->vid);
  foreach ($terms2 as $term2) {
    $subects[$term2->tid] = $term2->name;
  }
  
   
  	/** list all grade terms*/
	  $v = taxonomy_vocabulary_machine_name_load('test_grade');
  $terms = taxonomy_get_tree($v->vid);
  foreach ($terms as $term) {
    $grade_id[$term->tid] = $term->name;
  }
  
 	
	/////////////////////////////////

  	
	
	$form['gr_id']=array(
		'#type'=>'select',
		'#title'=>'Grade',
		'#options'=>$grade_id,
		'#required'=>TRUE,
		);
	
	$form['sub_id']=array(
		'#type'=>'select',
		'#title'=>'Subject',
		'#options'=>$subects,
		'#required'=>TRUE,
		);
	
	
	$form['full_marks']=array(
		'#type'=>'textfield',
		'#title'=>'Full Marks',
		'#required'=>TRUE,
	);

	$form['total_question']=array(
		'#type'=>'textfield',
		'#title'=>'Total Questions',
		'#description'=>'Enter number of question to show during test',
		'#required'=>TRUE,
	);
	
	
	
	$form['duration']=array(
		'#type'=>'textfield',
		'#title'=>'Test Duration',
		'#description'=>'Enter test duration in minute without typing "min"',
		 '#size'=>4,
		'#required'=>TRUE,
	);
	
	
	
	$form['pass_percentage']=array(
		'#type'=>'textfield',
		'#title'=>'Pass Percentage',
		'#size'=>4,
		'#description'=>'Enter numeric value without "%"',
		'#required'=>TRUE,
		 
		 
		
	);

	
	$form['submit']=array(
		'#type'=>'submit',
		'#default_value'=>'Submit', 
		
		);
		
		
		
		
		
		return $form;
	}


function test_setting_add_form_submit($form, &$form_state){
	$gr_id=$form_state['values']['gr_id'];
	$sub_id=$form_state['values']['sub_id'];
	$full_marks=$form_state['values']['full_marks'];
	$total_question=$form_state['values']['total_question'];
	$duration=$form_state['values']['duration'];
	$pass_percentage=$form_state['values']['pass_percentage'];
	
	$fields=array(
		'gr_id'=>$gr_id,
		'sub_id'=>$sub_id,
		'full_marks'=>$full_marks,
		'total_question'=>$total_question,
		'duration'=>$duration,
		'pass_percentage'=>$pass_percentage,
	);
	
	//dpm($fields);
	 $inserted=drupal_write_record('test_settings',$fields);
	 if($inserted!=''){
	 	drupal_set_message('Successfully Submited');
		drupal_goto('admin/test/test_setting');
	 }
	
}





function test_setting_list(){
	 
	$query=db_select('test_settings','ts');
	$query->fields('ts');
	
	$result=$query->execute();
	
	$header=array('Grade','Subject','Full marks','Total Questions','Pass percentage','Duration','Edit');
	$rows=array();
	foreach($result as $record){ 
		  
			$term_grade=taxonomy_term_load($record->gr_id);
			$grade=$term_grade->name;
		 
		 
		 
			$term_subject=taxonomy_term_load($record->sub_id);
			$subject=$term_subject->name;
		 
		 
		
		$rows[]=array($grade,$subject,$record->full_marks,$record->total_question,$record->pass_percentage,$record->duration.' min',l('edit','link'));
		
	}	
	
	$output=theme('table', array(
  'header' => $header,
  'rows' => $rows
  ));
 
	
	
	return $output;
}