<?php
 /**
 * Registering new vocabulary
 */
 
 function register_taxonomy_subject(){
 
 taxonomy_vocabulary_save((object) array(
  'name' => 'Test Subject',
  'machine_name' => 'test_subject',
));



$vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = 'test_subject'")->fetchField();

 
 
// Define the terms.
$terms['g1'][] = 'English';
$terms['g1'][] = 'Mathematics';
$terms['g1'][] = 'Physics';
$terms['g1'][] = 'Chemistry';
$terms['g1'][] = 'Biology';

 
foreach ($terms as $parent => $children) {
 
  foreach ($children as $term) {
    // Create the child term.
    taxonomy_term_save((object) array(
      'name' => $term,
      'vid' => $vid,
        ));
  }
}

}

 
 
 
 function register_taxonomy_grade(){
 
 taxonomy_vocabulary_save((object) array(
  'name' => 'Test Grade',
  'machine_name' => 'test_grade',
));



$vid = db_query("SELECT vid FROM {taxonomy_vocabulary} WHERE machine_name = 'test_grade'")->fetchField();

 
 
// Define the terms.
$terms['g1'][] = 1;
$terms['g1'][] = 2;
$terms['g1'][] = 3;
$terms['g1'][] = 4;
$terms['g1'][] = 5;
$terms['g1'][] = 6;
$terms['g1'][] = 7;
$terms['g1'][] = 8;
$terms['g1'][] = 9;
$terms['g1'][] = 10;
$terms['g1'][] = 11;
$terms['g1'][] = 12;



 
foreach ($terms as $parent => $children) {
 
  foreach ($children as $term) {
    // Create the child term.
    taxonomy_term_save((object) array(
      'name' => $term,
      'vid' => $vid,
        ));
  }
}

}
