<?php

function user_dashboard(){
	/**
		*Implements hook_user_login to redirect after login
		*/
		function tests_user_login(&$edit,$account){
			 $edit['redirect']='user_dashboard';
			
			 
			
		}
		
		
		
	if(user_is_logged_in()==TRUE){
	
	
	global $user;
	
	function take_test_settings($form, &$form_state){
			//list all subject terms////////////
	  $v2 = taxonomy_vocabulary_machine_name_load('test_subject');
  $terms2 = taxonomy_get_tree($v2->vid);
  foreach ($terms2 as $term2) {
    $subects[$term2->tid] = $term2->name;
  }
  
   
  	/** list all grade terms*/
	  $v = taxonomy_vocabulary_machine_name_load('test_grade');
  $terms = taxonomy_get_tree($v->vid);
  foreach ($terms as $term) {
    $grade_id[$term->tid] = $term->name;
  }
  
 	
	/////////////////////////////////

  	
	
	$form['grade']=array(
		'#type'=>'select',
		'#title'=>'Grade',
		'#options'=>$grade_id,
		);
	
	$form['subject']=array(
		'#type'=>'select',
		'#title'=>'Subject',
		'#options'=>$subects,
		);
	
	$form['submit']=array(
		'#type'=>'submit',
		'#default_value'=>'Take Test', 
		);
		
		
		
		
		
		return $form;
	}
	
	function take_test_settings_submit($form, &$form_state){
		global $user;
		
		function test_user_settings_func($uid){
			$query=db_select('test_user_settings','tus');
			$query->fields('tus')
				  ->condition('tus.uid',$uid,'=');
				  
			$result=$query->execute();
			$record=$result->fetchAssoc();
			
			return $record;
		}
		
		$record=test_user_settings_func($user->uid);
		$total_tests_taken=$record['total_tests_taken'];
		$total_tests_remaining=$record['total_tests_remaining'];
		
		$fields=array(
			'total_tests_taken'=>$total_tests_taken+1,
			'total_tests_remaining'=>$total_tests_remaining-1,
			'uid'=>$user->uid,
		);
		
 		 drupal_write_record('test_user_settings',$fields,'uid');
		
		$grade=$form_state['values']['grade'];
		$subject=$form_state['values']['subject'];
		$arg=$grade."/".$subject;
		 drupal_goto('user_dashboard/'.$arg.'/take_test');
		
	
	}
	
	function remaining_test($uid){
		$query=db_select('test_user_settings','tus');
		$query->fields('tus')
			  ->condition('tus.uid',$uid,'=');
			  
		$result=$query->execute();
		$record=$result->fetchAssoc();
		
		return $record['total_tests_remaining'];
	}
	
	 
	
	$remaining_test=remaining_test($user->uid);
	if($remaining_test<=0){
		$remaining_test='N/A';
	}
	$data='';
	$data.= 'Welcome '.$user->name.'<br/>';
	$data.='Remaining Test: '.'<b>'.$remaining_test.'</b>'.'<br/><br/>';
	
	global $base_url;
	$url=url($base_url.'/user_dashboard/result',array('query'=>array('tur_id'=>'all')));
	$data.='All Past Test: '.'<b>'.l('Show',$url).'</b>';
	$data.='<br/><br/>';
	 $form= drupal_get_form('take_test_settings');
	
	if($remaining_test>0){ 
	 $data.=drupal_render($form);
	}else{
		$data.='You do not have any Test available,<br/> Consult with administrator for more details';
	}
	return $data;
	
	}else{
	
		
		
		$login_form=drupal_get_form('user_login');
		return  drupal_render($login_form);
		
		
		
	
	}
}





function users_list(){
	 
	$query=db_select('test_user_settings','us');
	$query->fields('us');
	
	$result=$query->execute();
	
	$header=array('User','Total Payment','Accessed Grade','Total test','Total Tests Taken',' Total Tests Remaining','Edit');
	$rows=array();
	foreach($result as $record){
		$user=user_load($record->uid);
		$terms_arr=unserialize($record->accessed_grade_id);
		
		$grades='';
		foreach($terms_arr as $tid){
			$term=taxonomy_term_load($tid);
			$grades.=$term->name." , ";
		}
		
		$rows[]=array($user->name,'Rs.'.$record->total_payment,$grades,$record->total_test,$record->total_tests_taken,
		$record->total_tests_remaining,l('edit','admin/test/settings/'.$record->usid.'/edit'));
		
	}	
	
	$output=theme('table', array(
  'header' => $header,
  'rows' => $rows
  ));
 
	
	
	return $output;
}

 


function user_settings_add(){
	return drupal_get_form('user_settings_add_form');
}




function user_settings_add_form($form, &$form_state){
	
	/**to show all user except already listed in test_user_settings table*/
	
	$query2=db_select('test_user_settings','tus');
 	 $query2->fields('tus',array('uid'));
		   
	$result2=$query2->execute();
	
	$old_user_id=array();
	foreach($result2 as $old_user){
	 		$old_user_id[]=$old_user->uid;
		 
	
	}
	
	$query=db_select('users','u');
 	 
	$query->condition('u.uid',array(0,1),'NOT IN')
			->condition('u.uid',$old_user_id,'NOT IN')
		  ->fields('u',array('uid','name'));
		  
		  
	$result=$query->execute();
	
	$users_list=array();
	foreach($result as $user){
		 	$users_list[$user->uid]=$user->name;
		}
    

	/**list all grade terms */
	  $v = taxonomy_vocabulary_machine_name_load('test_grade');
  $terms = taxonomy_get_tree($v->vid);
  $grade_key=array();
  foreach ($terms as $term) {
    $accessed_grade_id[$term->tid] = $term->name;
	$grade_key[]=$term->tid;
  }
  
 	
	/////////////////////////////////


	$form['uid']=array(
		'#type'=>'select',
		'#title'=>'Select Users',
		'#options'=>$users_list,
		'#required'=>TRUE,	
	);
	
	$form['total_payment']=array(
		'#type'=>'textfield',
		'#title'=>'Total payment',
		'#size'=>4,
		'#required'=>TRUE,	
		);
	
	$form['accessed_grade_id']=array(
		'#type'=>'select',
		'#title'=>'Accessed Grades',
		'#options'=>$accessed_grade_id,
		'#multiple'=>TRUE,
		'#required'=>TRUE,
		'#default_value'=>$grade_key,	
		);
	
	
	$form['total_test']=array(
		'#type'=>'textfield',
		'#title'=>'Total Tests',
		'#description'=>t('Enter number of  test this user can take '),
		'#size'=>4,
		'#required'=>TRUE,	
		);
	
	 
	 $form['submit']=array(
	 	'#type'=>'submit',
		'#default_value'=>t('Submit'),
	 );
	
	
	
	return $form;
	
}


 
function user_settings_add_form_submit($form, &$form_state){
	$uid=$form_state['values']['uid'];
	$total_payment=$form_state['values']['total_payment'];
	$accessed_grade_id=$form_state['values']['accessed_grade_id'];
	$total_test=$form_state['values']['total_test'];
	
	$fields=array(
		'uid'=>$uid,
		'total_payment'=>$total_payment,
		'accessed_grade_id'=>serialize($accessed_grade_id),
		'total_test'=>$total_test,
		'total_tests_remaining'=>$total_test,
	);
 	$inserted_id=drupal_write_record('test_user_settings',$fields);
	
	if($inserted_id!=''){
		drupal_set_message('Successfully Submited !');
		drupal_goto('admin/test/settings');
	}
}




/**
*********************** for Edit *************************************
*/


function user_setting_edit($arg){
	return drupal_get_form('user_setting_edit_form',$arg);
}




function user_setting_edit_form($form, &$form_state,$arg){
	 
	/**to show all user except already listed in test_user_settings table*/
	
	$query=db_select('test_user_settings','tus');
 	 $query->fields('tus')
	 		->condition('tus.usid',$arg,'=');
		   
	$result=$query->execute();
	
	$record=$result->fetchAssoc();
	 
 	 $user=user_load($record['uid']);

	/**list all grade terms */
	  $v = taxonomy_vocabulary_machine_name_load('test_grade');
  $terms = taxonomy_get_tree($v->vid);
  
  foreach ($terms as $term) {
    $accessed_grade_id[$term->tid] = $term->name;
	
  }
  
  $selected_grade_id=unserialize($record['accessed_grade_id']);
 	
	$grade_key=array();
	foreach($selected_grade_id as $key=>$grade){
		$grade_key[]=$key;
	}
	/////////////////////////////////


	$form['uid']=array(
		'#type'=>'textfield',
		'#title'=>'User',
		'#default_value'=>$user->name,
		'#disabled'=>TRUE,	
	);
	
	$form['total_payment']=array(
		'#type'=>'textfield',
		'#title'=>'Total payment',
		'#size'=>4,
		'#required'=>TRUE,
		'#default_value'=>$record['total_payment'],	
		);
	
	$form['accessed_grade_id']=array(
		'#type'=>'select',
		'#title'=>'Accessed Grades',
		'#options'=>$accessed_grade_id,
		'#multiple'=>TRUE,
		'#required'=>TRUE,
		'#default_value'=>$grade_key,	
		);
	
	
	$form['total_test']=array(
		'#type'=>'textfield',
		'#title'=>'Total Tests',
		'#description'=>t('Enter number of  test this user can take '),
		'#size'=>4,
		'#required'=>TRUE,
		'#default_value'=>$record['total_test'],		
		);
	$form['total_test_previous']=array(
		'#type'=>'hidden',
		'#default_value'=>$record['total_test'],
	);
	
	$form['total_test_taken']=array(
		'#type'=>'textfield',
		'#title'=>'Total Tests taken',
		 '#size'=>4,
		'#disabled'=>TRUE,
		'#default_value'=>$record['total_tests_taken'],		
		);
	
	
	
	$form['total_tests_remaining']=array(
		'#type'=>'textfield',
		'#title'=>'Total Tests remaining',
		 #size'=>4,
		'#disabled'=>TRUE,
		'#default_value'=>$record['total_tests_remaining'],		
		);
	
	
	$form['usid']=array(
		'#type'=>'hidden',
		'#default_value'=>$arg,
	);
	 
	 $form['submit']=array(
	 	'#type'=>'submit',
		'#default_value'=>t('Submit'),
	 );
	
	
	
	return $form;
	
}



function user_setting_edit_form_validate($form, &$form_state){
	$usid=$form_state['values']['usid'];
	$total_payment=$form_state['values']['total_payment'];
	$accessed_grade_id=$form_state['values']['accessed_grade_id'];
	$total_test=$form_state['values']['total_test'];
	
	$total_test_taken=$form_state['values']['total_test_taken'];
	
	$total_tests_remaining=$form_state['values']['total_tests_remaining']+( $form_state['values']['total_test']-$form_state['values']['total_test_previous']);
	
 	if($total_test<$total_test_taken){
		form_set_error('','Total test can not be less than total test taken !');
	}
 
}



 
function user_setting_edit_form_submit($form, &$form_state){
	$usid=$form_state['values']['usid'];
	$total_payment=$form_state['values']['total_payment'];
	$accessed_grade_id=$form_state['values']['accessed_grade_id'];
	$total_test=$form_state['values']['total_test'];
	
	$total_tests_remaining=$form_state['values']['total_tests_remaining']+( $form_state['values']['total_test']-$form_state['values']['total_test_previous']);
	
	$fields=array(
		'total_payment'=>$total_payment,
		'accessed_grade_id'=>serialize($accessed_grade_id),
		'total_test'=>$total_test,
		'total_tests_remaining'=>$total_tests_remaining,
		'usid'=>$usid,
	);
	
	//dpm($fields);
	
 	$inserted_id=drupal_write_record('test_user_settings',$fields,'usid');
	
	if($inserted_id!=''){
		drupal_set_message('user Setting Successfully Updated!');
		//drupal_goto('admin/test/settings');
	}
}


/**
*********************** for Edit *************************************
*/