<?php
/**
*Implements hook_block_info();
*/



function ads_block_info(){
	$blocks['ads_block']=array(
		'info'=>'Ads Block'	
	);
	
 	return $blocks;
	
}


/**
*Implements hook_block_configure
*/

function ads_block_configure($delta=''){
		
		switch($delta){
			case "ads_block":
			
			$block_load=block_load('ads','ads_block');
 			
			 // dpm($block_load);
			
			$form['ads_block']=array(
				'#type'=>'fieldset',
				'#title'=>'Ads Block setting'
			);
			
			
			$query=new EntityFieldQuery();
			$query->entityCondition('entity_type','ads');
			$result=$query->execute();
			
			$ads_1=entity_load('ads',array_keys($result['ads']));
			
			 
			$options=array();
			foreach($ads_1 as $value){
				$params=array(
					'style_name'=>'thumbnail',
					'path'=>$value->image,
					'width'=>'',
					'height'=>''
				);
			
				$options[$value->aid]="<div style='width:155px;float:left;'><br/><div style='width:125px;height:140px;border:5px solid #ccc;padding:4px;margin:4px;float:left'>".$value->title."<br/>".theme_image_style($params)."</div></div>";
			}
			
			
			 
			 
			 ////////Get selected ads/////////////////////////////
			 
			$query2 = db_select('ads_enabled', 'a');
			 
			$query2->condition('a.region',$block_load->region, '=')
				->fields('a',array('id','region','aid_arr'));
  
			$result2=$query2->execute();
			 
			 
			$options_checked=array(); 
			
			$id='';
			global $id;
			
			foreach($result2 as $value){
			 $options_checked=unserialize($value->aid_arr);
			 $id=$value->id;
			 }
			   //dpm("ID".$id);
			 
			 
			 //////////end //////////////////////////////////////
			 
			
			$form['ads_block']['select_ads']=array(
				'#title'=>'Select Ads',
				'#type'=>'checkboxes',
				'#options'=>$options,
				//'#default_value'=>$options_checked,
				'#default_value'=>$options_checked,
				'#suffix'=>'<div style="clear:both"></div>' 
			);
			
			/*
			 $regions=system_region_list('bartik',REGIONS_ALL);
			
			
		//	$regions=array_keys($regions_1);
				
			$form['ads_block']['select_region']=array(
				'#type'=>'select',
				'#title'=>'Select regions',
				'#options'=>$regions
			); 
			*/
			
			 
			return $form;
			
		}

}


 

/**
*Implements hook_block_save
*/

function ads_block_save($delta='',$edit=array()){
	switch($delta){
		case "ads_block":
		
			
			//dpm(get_defined_vars());
			 //dpm($edit);

 		
 		// watchdog('Blocks',$edit['regions']['bartik']);
		
 		
		$aid_arr=$edit['select_ads'];
		
		$aid_str=serialize($aid_arr);
		
		$block_load=block_load('ads','ads_block');
		
		$fields=array(
			'bid'=>$block_load->bid,
			'region'=>$edit['regions']['bartik'],
			'aid_arr'=>$aid_str,
			
		);
		
		 global $id;
 		 if($id !=''){
			$fields['id']=$id;
			drupal_write_record('ads_enabled',$fields,'id');
		}else{
			drupal_write_record('ads_enabled',$fields);
		}
		 
		
		 dpm("IDD".$id);
	
	}
}


/**
*Implements hook_block_view()
*/

function ads_block_view($delta=''){
	switch($delta){
		case "ads_block":
		$block['subject']='Ads';
		
 		$block_load=block_load('ads','ads_block');
		//dpm($block_load);
		
		
 			 
			$query2 = db_select('ads_enabled', 'a');
			 
			$query2->condition('a.bid',$block_load->bid, '=')
				->fields('a',array('id','bid','region','aid_arr'));
  
			$result2=$query2->execute();
			 
			 
 			 $data='';
			foreach($result2 as $value){
			 $selected_ads=unserialize($value->aid_arr);
			  
			 }
			 
			 foreach($selected_ads as $id){
						
						$query = db_select('ads', 'a');
			 
						$query->condition('a.aid',$id, '=')
							->fields('a',array('aid','title','image'));
			  
						$result=$query->execute();
							
									 
						 	
					
						
						 foreach($result as $ads){
				 			$params=array(
							'style_name'=>'thumbnail',
							'path'=>$ads->image,
							'width'=>'',
							'height'=>''
						);
						 	$data.=theme_image_style($params)."<br/><br/>";
						 }
						
								
				}
				
				$block['content']=$data;
			   //dpm("ID".$id);
			 
			 
 		
		
	}
	
	return $block;

}























