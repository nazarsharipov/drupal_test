<?php
function question_list(){
	 
	$query=db_select('test_questions','q');
	$query->fields('q');
	  $query = $query->extend('TableSort')->extend('PagerDefault')->limit(50);

	$result=$query->execute();
	
	$header=array('Grade','Subject','Question','Created Date','Edit');
	$rows=array();
	foreach($result as $record){ 
		  
			$term_grade=taxonomy_term_load($record->gr_id);
			$grade=$term_grade->name;
		 
		 
		 
			$term_subject=taxonomy_term_load($record->sub_id);
			$subject=$term_subject->name;
		 
		 
 		$rows[]=array($grade,$subject,$record->title,'','');
		
	}	
	
	$output=theme('table', array(
  'header' => $header,
  'rows' => $rows
  ));
 
	$output.=theme('pager');
	
	return $output;
}


function question_add(){
	return drupal_get_form('question_add_form');
}

function question_add_form($form, &$form_state){
	
	//to show all user////////
	$query=db_select('users','u');
	$query->fields('u',array('uid','name'))
		->condition('u.uid',array(0,1),'NOT IN');
	
	$result=$query->execute();
	
	foreach($result as $user){
		$users_list[$user->uid]=$user->name;
	}
    

	/**list all subject terms*/
	  $v2 = taxonomy_vocabulary_machine_name_load('test_subject');
  $terms2 = taxonomy_get_tree($v2->vid);
  foreach ($terms2 as $term2) {
    $subects[$term2->tid] = $term2->name;
  }
  
   
  	/** list all grade terms*/
	  $v = taxonomy_vocabulary_machine_name_load('test_grade');
  $terms = taxonomy_get_tree($v->vid);
  foreach ($terms as $term) {
    $grade_id[$term->tid] = $term->name;
  }
  
 	
	/** End */
   	
	
	$form['grade']=array(
		'#type'=>'select',
		'#title'=>'Grade',
		'#options'=>$grade_id,
		'#required'=>TRUE,
		);
	
	$form['subject']=array(
		'#type'=>'select',
		'#title'=>'Subject',
		'#options'=>$subects,
		'#required'=>TRUE,
		);
		
	$form['title']=array(
		'#type'=>'textfield',
		'#title'=>'Question Name',
		'#required'=>TRUE,
 		 
		);
	
	$header=array('Options','Right Answer');
	for($i=1;$i<=4;$i++){
		$rows[]=array('<input required  style="width:90%;height:31px;border:1px solid #333;padding-left:12px;" type="textfield" name="options[]" 
		  />','<label style="width:100px;"><input required type="radio" name="right_ans" value="'.($i-1).'" /></label>');
	}
	$questions=theme('table',array(
	'header'=>$header,
	'rows'=>$rows,
	));
	
	
	$form['questions']=array(
		'#markup'=>$questions,
	);
	
	 
	 $form['submit']=array(
	 	'#type'=>'submit',
		'#default_value'=>t('Submit'),
	 );
	
	
	
	return $form;
	
}

 

function question_add_form_submit($form, &$form_state){
	$gr_id=$form_state['values']['grade'];
	$sub_id=$form_state['values']['subject'];
	$title=$form_state['values']['title'];
	$ans_options=$_POST['options'];
	$right_ans=$form_state['input']['right_ans'];
	// dpm($form_state);
	
	
	$fields=array(
		'gr_id'=>$gr_id,
		'sub_id'=>$sub_id,
		'title'=>$title,
		'ans_options'=>serialize($ans_options),
		'right_ans'=>$right_ans
	);
	
	//dpm($fields);
	
	 $inserted_id=drupal_write_record('test_questions',$fields);
	
	if($inserted_id!=''){
		drupal_set_message('Question Successfully Submited !');
	}
}



