<?php

/*-------------------------- PART I CTOOLS WIZARD IMPLMENTATION ---------------------- */

/**
 * menu callback for the multistep form 
 * step is whatever arg one is -- and will refer to the keys listed in
 * $form_info['order'], and $form_info['forms'] arrays
 */
function wombat_wizard_modal() {
  ctools_include('wizard');
  ctools_include('ajax');
  ctools_include('modal');
  $form_info = array(
    // think of this ID like a "sub" hook
    'id' => 'wombat_generator',
    // if the url was wombat/tools with arg 2 being the step
    // we'd write wombat/tools/step
    'path' => "wombat/modal-wizard/%step",
    // a trail shows the user where they are in the 
    'show trail' => TRUE,
    // a back button 
    'show back' => TRUE,
    'modal' => TRUE,
    // a "reset" button that returns the user to the first step and clears the form's stored values
    'show cancel' => true,
    // not really important for our purposes
    'show return' =>false,
    // these buttons can have custom text if needed
    'next text' => 'Proceed to next step',
    // these are yoru callback functions for the buttons -- you pass them a form_state in most cases, and use ctools storage
    // instead of the $form_state[storage]
    
    // while the below callbacks *look* like hooks, they actually can be named anything
    // at the bottom of the file is an example of how these should look that will work in nearly any case
    'next callback' =>  'wombat_generator_add_subtask_next',
    'finish callback' => 'wombat_generator_add_subtask_finish',
    'return callback' => 'wombat_generator_add_subtask_finish',
    'cancel callback' => 'wombat_generator_add_subtask_cancel',
    'order' => array(
      // this title gives the step a name in the stepper
      // note that the order of these arrays is what controls the order of steps,
      'wombat_add_form' => t('Step 1: Create Wombat'),
      'wombat_deployment_form' => t('Step 2: Deploy Wombat'),
    ),
    'forms' => array(
      'wombat_add_form' => array(
         // see docs in advanced help for more options here such as file path
        'form id' => 'wombat_add_form'
      ),
      'wombat_deployment_form' => array(
        'form id' => 'wombat_deployment_form'
      ),
    ),
  );
  // **  alyways load the stored values 
  $wombat = wombat_generator_get_page_cache('');
  // if there is none set up the default storage object
  if (!$wombat) {
    // set form to first step -- we have no data
    $step = current(array_keys($form_info['order']));
    // initialize your storage object
    $wombat = new stdClass();
    // all battlewombats are fuzzy, fyi
    $wombat->fur_texture = 'Very fuzzy';
    // ** set the storage object so its ready for whatever comes next
    wombat_generator_set_page_cache($wombat);
  }

  $form_state = array(
    'cache name' => '',
    'cstorage' => $wombat,
    'ajax' => TRUE,
    'modal' => TRUE,
    'commands' => array(),
  );
  
  // **  this is the magic function that does the tricks
  $output = ctools_wizard_multistep_form($form_info, $step, $form_state);
  print $output;
  exit();
}
function wombat_wizard($step = null) { 
    ctools_include('ajax');
    ctools_include('modal');
    ctools_modal_add_js();

    return ctools_ajax_text_button("test", 'wombat/modal-wizard', $alt, $class, 'ctools-use-modal');

  // required include for wizard

  
}

/*-------------------------- PART 2 JUST CLASSIC FORMAPI  ---------------------- */

/**
 * All forms within this wizard will take $form, and $form_state by reference
 * note that the form doesn't have a return value.
 */
function wombat_add_form(&$form, &$form_state) {
  $wombat = &$form_state['cstorage'];
  $form['name'] = array(
    '#type' => 'textfield',
    '#required' => 1,
    '#title' => 'Wombat name',
    '#default_value' => $wombat->name,
  );
  $form['temperment'] = array(
    '#title' => 'Temperment',
    '#type' => 'radios',
    '#required' => 1,
    '#default_value' => $wombat->temperment,
    '#options' => array(
      'docile' => 'Docile',
      'irritable' => 'Irritable',
      'dangerous' => 'Dangerous',
    )
  );
  $form_state['no buttons'] = TRUE; 
}

/**
 * Note that this validate callback operates exactly like it does in the regular form api
 */
function wombat_add_form_validate(&$from, &$form_state) {
  if ($form_state['values']['name'] == 'billy') {
    form_set_error('name', 'No wombat is allowed to be named billy!');
  }
}

/**
 * KEY CONCEPT: generally, you will never save data here -- you will simply add values to the 
 * yet to be saved ctools_cache object. 
 * 
 * Saving happens at the end, within the $form_info['finish callback'];
 */
function wombat_add_form_submit(&$from, &$form_state) {
  $submitted = $form_state['values'];
  $save_values = array('name', 'temperment'); 
  foreach($save_values as $value) {
    // set it in cstorage, the voodoo in part 3 will take care of the rest magically
    $form_state['cstorage']->$value = $submitted[$value];
  }
}


function wombat_deployment_form(&$form, &$form_state) {
  $wombat = &$form_state['cstorage'];
  $dutys = array(
    'fuzzball' => 'Provide a fuzzball',
    'noise-maker' => 'Make irritating noises'    
  );
  /* we give different duty options based on wombat temper */
  switch($wombat->temperment) {
    case 'docile':
      // only docile wombats allow themselves to become fatballs
      $dutys['fatball'] = 'Sit around and be fat';

      break;
    case 'irritable':
      $dutys['menace'] = 'Basic Menace Duties';
      break;
    case 'dangerous':
      $dutys['menace'] = 'Basic Menace Duties';
      $dutys['scratch'] = "Scratch warfare";
      $dutys['bite'] = "Bite warfare";
      break;
  }
  $form['name'] = array(
    '#type' => 'item',
    '#title' => 'Wombat name',
    '#value' => $wombat->name,
  );
  $form['temper'] = array(
    '#type' => 'item',
    '#title' => 'Temperment',
    '#value' => $wombat->temperment,
  );
  $form['duties'] = array(
    '#type' => 'checkboxes',
    '#tree' => true,
    '#options' => $dutys,
    '#title' => 'Duties',
    '#value' => $wombat->duties,
    '#required' => 1,
  );
  $form_state['no buttons'] = TRUE; 
}


/**
 * Same idea as previous steps submit
 */
function wombat_deployment_form_submit(&$form, &$form_state) {
  $duties = $form_state['values']['duties'];
  $form_state['cstorage']->duties = $duties;
}



/*----PART 3 CTOOLS CALLBACKS -- these usually don't have to be very unique  ---------------------- */

/**
 * Callback generated when the add page process is finished.
 * this is where you'd normally save.
 */
function wombat_generator_add_subtask_finish(&$form_state) {
  $wombat = &$form_state['cstorage'];
  drupal_set_message('Wombat '.$wombat->name.' successfully deployed. Wombat will be assigned the following duties: '.implode(",", $wombat->duties)).'.';
  // Clear the cache
  wombat_generator_clear_page_cache($form_state['cache name']);
}

/**
 * Callback for the proceed step
 *
 */
function wombat_generator_add_subtask_next(&$form_state) {
  $wombat = &$form_state['cstorage'];
  ctools_include('object-cache');
  $cache = ctools_object_cache_set('wombat_generator', $form_state['cache name'], $wombat);
}

/**
 * Callback generated when the 'cancel' button is clicked.
 *
 * All we do here is clear the cache.
 */
function wombat_generator_add_subtask_cancel(&$form_state) {
  // Update the cache with changes.
  wombat_generator_clear_page_cache($form_state['cache name']);
}

/*----PART 4 CTOOLS FORM STORAGE HANDLERS -- these usually don't have to be very unique  ---------------------- */

/**
 * Store changes to a task handler in the object cache.
 */
function wombat_generator_set_page_cache($wombat) {
  ctools_include('object-cache');
  ctools_object_cache_set('wombat_generator', $form_state['cache name'], $wombat);
}

/**
 * Remove an item from the object cache.
 */
function  wombat_generator_clear_page_cache($name) {
  ctools_include('object-cache');
  ctools_object_cache_clear('wombat_generator', $name);
}

/**
 * Get the cached changes to a given task handler.
 */
function wombat_generator_get_page_cache($name) {
  ctools_include('object-cache');
  $cache = ctools_object_cache_get('wombat_generator', $name);
  return $cache;
}




function d($in) {
  print '<pre>';
  print_r($in);
  print '</pre>'; 
}